
#加油类app调研


## Refuel - WA Fuel Prices & Station Finder

只有西澳地区，没有东海岸城市的价格，实时显示今日和明日油价，涵盖各类加油站300多个，汽油和柴油价格均有。数据来自[这个](http://www.fuelwatch.wa.gov.au/fuelwatch/pages/home.jspx)站点，这是澳洲商务部的一个非营利性站点，数据免费。App就是提供搜索和展示加油站以及油价功能。个人开发者，app售价 $2.99。也有对应的[web app](https://wafuelfinder.com/)，免费，功能类似。


itunes 地址 [https://itunes.apple.com/nz/app/refuel-wa-fuel-prices-station-finder/id604701577?mt=8]()

### Screenshots

![](https://is3-ssl.mzstatic.com/image/thumb/Purple111/v4/e0/70/e8/e070e885-2e73-e092-9ae3-bc6532cbfbba/source/750x750bb.jpeg)

![](https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/7e/70/f6/7e70f6a2-1bba-20f8-3422-6cbc5968c2ab/source/750x750bb.jpeg)

![](https://is2-ssl.mzstatic.com/image/thumb/Purple122/v4/70/b3/e1/70b3e1d3-9fb1-0247-e0e4-291c95a16c31/source/750x750bb.jpeg)

### 最初发布-最后更新时间

* Feb 26, 2013
* Apr 16, 2017

## Fuel Watch New Zealand

新西兰全国的大多数加油站油价都可以看到，不过这个价格是用户来更新，所以不少地区的油价不准，或者有的加油站都没有了，在地图上还能看到。评论里面比较多的就是，非市区很多加油站没有数据，再一个就是油价参考价值不大。所以评分里面，1分占了不少。我安装看了下，功能很简单，地图上一堆点，点击显示油价和最后更新时间。我在北岸看的一些，80%都是1-2个月以前的了。如果需要驾车前往之类的，直接给你导到iphone自带的地图上，这个可以借鉴下。非个人开发者。

itunes 地址 [https://itunes.apple.com/nz/app/fuel-watch-new-zealand/id632937174?mt=8](https://itunes.apple.com/nz/app/fuel-watch-new-zealand/id632937174?mt=8)

## Screenshots

![](https://is1-ssl.mzstatic.com/image/thumb/Purple71/v4/ab/3e/16/ab3e162b-d2d8-98cd-b249-dfe427ae05d2/source/750x750bb.jpeg)

![](https://is5-ssl.mzstatic.com/image/thumb/Purple71/v4/0a/85/4f/0a854ff3-71f7-ca6e-a6e5-a46b3a45cab3/source/750x750bb.jpeg)

![](https://is3-ssl.mzstatic.com/image/thumb/Purple71/v4/0b/83/24/0b8324cc-89aa-b4a6-26ae-f11498570d09/source/750x750bb.jpeg)

### 最初发布-最后更新时间

* Otc 15, 2014
* Sep 21, 2016

## BPMe

BP官方出品的app，主要功能是在bp加油站加油可以预先支付，地图可以展示旗下所有的加油站和联系方式，不显示油价，应该就是可以通过app里面的aa加油卡或者visa快捷支付，没其他功能。UI和功能都做的比较好看，一看就是大厂作品。

itunes 地址 [https://itunes.apple.com/nz/app/bpme-pay-for-fuel-from-your-car-with-bpme/id1116524739?mt=8](https://itunes.apple.com/nz/app/bpme-pay-for-fuel-from-your-car-with-bpme/id1116524739?mt=8)

## Screenshots

![](https://is5-ssl.mzstatic.com/image/thumb/Purple111/v4/af/f3/a3/aff3a3c2-a5fa-3614-43d7-d34d13febf5e/source/750x750bb.jpeg)

![](https://is5-ssl.mzstatic.com/image/thumb/Purple122/v4/0c/ed/da/0ceddaf9-ed02-644d-f966-e28baef35d0e/source/750x750bb.jpeg)

![](https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/b9/c3/76/b9c3761a-2c41-4c21-13fc-af53fc96b786/source/750x750bb.jpeg)


### 最初发布-最后更新时间

* Jul 24, 2016
* Apr 5, 2017

# GasBuddy

北美资深加油站价格app，7年历史，11m用户，全职雇员超过100人，靠用户自觉更新，有一系列用户奖惩措施，采用积分策略，更新多少分，confirm别人的价格多少分之类的，可以用积分换一些优惠的服务或者实体券，而且每天会有1人免费获得$100的加油券，但是必须要做任务，类似“更新附近的3个汽油价格”，“确认周围1个柴油价格”这种任务。数据更新很及时，加油站覆盖面广。不过此app的android版本和ios版本，都必须美国地区用户安装，我没有美国区账号，没法安装体验，只能从截图、介绍和youtube上看，youtube上的视频都是4-5年前的了。但是这个应该是所有靠社区用户维护数据的app里面做的最成功的。这个app也有个差不多的web版本的[页面](http://www.twincitiesgasprices.com/GasPriceSearch.aspx)。看了一下，就是给你点数，完成各种任务，获得点数，想赢免费加油卡，1000点入门费，还不保证能获得，似乎每天就一位。商业模式，除了广告也能为大客户提供一些选址和定向投放的工作，还在做支付平台。从它的招聘职位上就能看出来，下一步主要还是忘商务上搞，数据挖掘数据分析方面寻找商机。整个用户激励做的很好，加入了很多社交元素，用户愿意去自主维护整个平台的发展，在轻松愉悦的环境下就把油价的事情干了。。。目前的产品涵盖桌面、ios、android、winphone、iwatch，非常丰富，疯狂扩张中。

itunes 地址 [https://itunes.apple.com/us/app/gasbuddy-find-cheap-gas/id406719683?mt=8](https://itunes.apple.com/us/app/gasbuddy-find-cheap-gas/id406719683?mt=8)


#screenshots

![](https://is1-ssl.mzstatic.com/image/thumb/Purple122/v4/c3/00/25/c3002520-ed2f-f80d-9b82-3ef27d84235f/source/750x750bb.jpeg)

![](https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/ba/c0/f6/bac0f6cf-ac33-7005-a968-ca72acecefd7/source/750x750bb.jpeg)

![](https://is1-ssl.mzstatic.com/image/thumb/Purple111/v4/df/34/7f/df347ff0-63c4-2d78-feee-7bbb1fd54152/source/750x750bb.jpeg)

![](https://is3-ssl.mzstatic.com/image/thumb/Purple122/v4/e0/ab/67/e0ab671e-705e-a8db-08b2-1de94392ff4b/source/750x750bb.jpeg)

### 最初发布-最后更新时间

* Dec 17, 2010
* Apr 11, 2017


## 其他App

* Fuel Finder Free 和Refuel同样的套路。只有西澳地区的油价，已经不更新了。个人开发者，免费，开发者是个中国人，应该是个练手项目。。。
* Fuelman Mobile Locator 美国的加油站和价格展示，同样的问题，数据更新不及时，用户评论区各种骂娘。
* AA smartfuel AA官方出品，似乎很多bug，90%的评论是1星，评论区已被骂平。


## Conclusion

* 这类展示加油站和价格的app，各个国家和地区都有，从互联网时代就出现了。
* 套路就几种，一种是利用其它平台的现有价格，例如澳洲的那些app，另外一种就是靠用户自觉更新（剩下的那种），还一种是那种卖卡的平台，和加油站合作，用他们的卡去支付，这样他们可以自己算出来油价，也是很准确的，也有app平台之类的，nz就有人在做。咱商量的套路我还没看到有在用的，这个思路别人一定想到过，但是为何不采用这是我们要考证的问题。
* 通过用户自己来更新油价的app是有很多的，nz目前最少3款，不过最严重的问题就是更新不及时，用户粘性太小，总是来查看价格，却不愿意更新价格，是这类app的共性。
* gasbuddy目前主要是美国市场，加拿大也开始搞，不过根据他们的技术和经验，部署大洋洲这边的业务应该是分分钟的事情。