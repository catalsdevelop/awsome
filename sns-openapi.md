用户验证和退出功能已经上线
所有接口

## GET /openapi/simplelogin

iframe方式，querystring里面给定returnUrl，可用于登录成功后的跳转，在window.name获取返回值
```js
{
  "uuid":"d024260e-95ac-4194-8adb-4c70b7908650",
  "ticket":"eb279276-394a-42f5-9144-836be7628b8e",
}
```
其中，uuid和ticket用于验证用户是否有效

## GET /openapi/loginstatus

iframe方式，用来验证当前用户是否已经登录，返回值同上，如果没有登录，返回值为null

## GET /openapi/validate

用户服务端验证用户合法性，可以直接http请求或者ajax请求

/openapi/validate?uuid=d024260e-95ac-4194-8adb-4c70b7908650&ticket=0d4adf97-9123-4834-abe5-66c832a71f81

需要传递两个参数，分别是登录时获取的参数，返回值

```js
{
    "err_code": 0,
    "userInfo": {
        ...
    }
}
```

```js
{
    "err_code": -1,
}
```

## POST /openapi/logout
用户服务端退出登录的用户，可以直接http请求或者ajax请求，request的header需要json格式，设置content-type为'application-json'，request body使用json格式提交

```js
{
  "uuid":"d024260e-95ac-4194-8adb-4c70b7908650",
  "ticket":"0d4adf97-9123-4834-abe5-66c832a71f81"
}
```

返回值
```js
{
    "err_code": 0, 0|-1
}
```