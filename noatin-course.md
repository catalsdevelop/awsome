课程需要的url或者接口

## 诺丁平台

### 创建课程时跳转url /course/new/:courseId?name=xxxxx

* name 课程名称，使用urlencode编码


## 编码平台

### 发布之前去编码平台确认 GET /course/publish/:courseId

返回值：
```js
{
  result: 0, // 0成功，-1失败
  msg: ''    // 额外
}
```

### 审核之前去编码平台确认 GET /course/approvalCourse/:courseId/:action

action取值 `pass`|`deny`

返回值：
```js
{
  result: 0, // 0成功，-1失败
  msg: ''    // 额外
}
```

### 获取某个课程的目录 GET /course/getSuccessCourse/:courseId

```js
{
   categories: [
    '第一节 演员的自我修养',
    '第一节 演员的自我修养',
    '第一节 演员的自我修养',
   ]
}
```
